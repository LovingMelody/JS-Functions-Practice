// 1.
// Define a function max() that takes two numbers as arguments and returns the largest of them. Use the if-then-else construct available in JavaScript.
// Then, write and example of using the function.

function max(numA, numB){if (numA > numB){return numA}else{return numB}}
console.log(`Max of 0..100 is ${max(0, 100)}`);


// 2.
// Define a function maxOfThree() that takes three numbers as arguments and returns the largest of them.
// Then, write and example of using the function.

function maxOfThree(numA, numB, numC){return max(max(numA, numB), numC)}
console.log(`Of 22, 48 and 32 ${maxOfThree(22, 48, 32)} is the largest number`);

// 3.
// Write a function that takes a character (i.e. a string of length 1) and returns true if it is a vowel, false otherwise.
// Then, write and example of using the function.

function isVowel(char){return ['a','e','i','o','u','y'].includes(char.toLowerCase());}
console.log(`The vowels in \"Once Upon a time, there was a duck\" are: ${"Once Upon a time, there was a duck".split('').filter(isVowel).join(', ')}`);

// 4.
// Write a function called `sum` that takes two parameters and returns the sum of those 2 numbers.
// Then, write and example of using the function.
function sum(numA, numB){return numA+numB}
console.log(`The sum of 22 and 82 is ${sum(22, 82)}`)


// 5.
// Write a function named `avg` that takes 3 parameters and returns the average of those 3 numbers.
// Then, write and example of using the function.
function avg(numA, numB, numC) {return sum(sum(numA, numB), numC)/3}
console.log(`The average of 82, 78, and 94 is ${avg(82, 78, 94)}`)


// 6.
// Write a function called `getLength` that takes one parameter (a string) and returns the length
// Then, write and example of using the function.
function getLength(text) {return text.length};
console.log(`pneumonoultramicroscopicsilicovolcanoconiosis is ${getLength("pneumonoultramicroscopicsilicovolcanoconiosis")} characters long`);


// 7.
// Write a function called `greaterThan` that takes two parameters
// and returns `true` if the second parameter is greater than the first.
// Otherwise the function should return `false`.
// Then, write and example of using the function.
function greaterThan(paramA, paramB) {return paramB > paramA}
console.log(`It is ${greaterThan(48, 53)} that 53 < 48`);


// 8.
// Write a function called `greet` that takes a
// single parameter and returns a string that
// is formated like "Hello, Name!" where *Name*
// is the parameter that was passed in.
// Then, write and example of using the function.
function greet(name) {return `Hello, ${name}!`}
console.log(greet("Bob"));


// 9.
// Write a function called `madlib` that takes 4 or more parameters (words).
// The function should insert the words into a pre-defined sentence.
// Finally the function should return that sentence.
// Note: When I say words and sentence I mean strings. For example:
// words: "quick", "fox", "fence"
// sentence: "quick brown fox jumps over the fence"
// Then, write and example of using the function.
function madlib(wordA, wordB, wordC, wordD) {return `The ${wordA} ${wordB} jumped over the ${wordC} to eat ${wordD}`}
console.log(madlib("Fat", "Fox", "fence", "chickens"))

// 10.
// Write a function rovarspraket() that will translate a text into "rövarspråket". That is, double every consonant and place an occurrence of "o"// in between. For example, translate("this is fun") should return the string "tothohisos isos fofunon".
function rovarspraket(phrase){return phrase.split('').map(l => 'bcdfghjklmnpqrstvxz'.includes(l.toLowerCase()) ? `${l}o${l}`: l).join('')}
console.assert(rovarspraket("this is fun")=="tothohisos isos fofunon")
